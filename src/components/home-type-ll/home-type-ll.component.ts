import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-type-ll',
  templateUrl: './home-type-ll.component.html',
  styleUrls: ['./home-type-ll.component.scss']
})
export class HomeTypeLlComponent implements OnInit {

  public imageSlider: any[] = [
    'assets/images/image-1.jpg',
    'assets/images/image-2.jpg',
    'assets/images/image-3.jpg',
    'assets/images/image-4.jpg',
    'assets/images/image-5.jpg'
  ];

  
  constructor() { }

  ngOnInit() {
  }

}
