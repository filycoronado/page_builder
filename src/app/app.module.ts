import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from '../components/components.module';
import { HomeComponentComponent } from 'src/Pages/home-component/home-component.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { Home_type_1Component } from 'src/components/home_type_1/home_type_1.component';
import { HomeTypeLlComponent } from 'src/components/home-type-ll/home-type-ll.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    Home_type_1Component,
    HomeTypeLlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    ComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
